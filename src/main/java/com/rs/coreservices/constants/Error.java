package com.rs.coreservices.constants;

public enum Error {

	SERVER("SERVER", 10000, "Server error"), CLIENT("CLIENT", 100, "Bad request");

	private String name;
	private int code;
	private String userMessage;

	private Error(String name, int code, String userMessage) {
		this.name = name;
		this.code = code;
		this.userMessage = userMessage;
	}

	public String getName() {
		return name;
	}

	public int getCode() {
		return code;
	}

	public String getUserMessage() {
		return userMessage;
	}

}
