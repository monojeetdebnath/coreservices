package com.rs.coreservices.utils;

import java.util.ArrayList;
import java.util.List;

public class CoreServiceCollectionUtils {

	public static <E> List<E> iterableToList(Iterable<E> iter) {
		List<E> list = new ArrayList<E>();
		iter.forEach(list::add);
		return list;
	}

}
