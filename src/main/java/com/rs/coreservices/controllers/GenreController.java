package com.rs.coreservices.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rs.coreservices.dto.response.GenreDto;
import com.rs.coreservices.services.GenreService;

@RestController
@RequestMapping("/v1/{categoryId}/genres")
public class GenreController {

	@Autowired
	private GenreService genreService;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public List<GenreDto> getAllGenres(@PathVariable String categoryId) {
		return genreService.findAll(categoryId);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public GenreDto createGenre(@PathVariable String categoryId, @RequestBody GenreDto genreDto) {
		return genreService.save(categoryId, genreDto);
	}

	@RequestMapping(value = "/{genreId}", method = RequestMethod.DELETE)
	public void deleteGenre(@PathVariable String categoryId, @PathVariable String genreId) {
		genreService.delete(categoryId, genreId);
	}

}