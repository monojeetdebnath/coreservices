package com.rs.coreservices.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rs.coreservices.dto.response.CategoryDto;
import com.rs.coreservices.services.CategoryService;

@RestController
@RequestMapping("/v1/categories")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public List<CategoryDto> getAllCategories() {
		return categoryService.findAll();
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public CategoryDto createCategory(@RequestBody CategoryDto categoryDto) {
		return categoryService.save(categoryDto);
	}

	@RequestMapping(value = "/{categoryId}", method = RequestMethod.DELETE)
	public void deleteCategory(@PathVariable String categoryId) {
		categoryService.deleteByCategoryId(categoryId);
	}

}