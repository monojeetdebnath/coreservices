package com.rs.coreservices.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.rs.coreservices.constants.Error;
import com.rs.coreservices.dto.response.GlobalErrorDto;

@ControllerAdvice
public class GlobalExceptionHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	@ExceptionHandler(value = EmptyResultDataAccessException.class)
	public ResponseEntity<GlobalErrorDto> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex) {
		return new ResponseEntity<GlobalErrorDto>(new GlobalErrorDto(Error.CLIENT.getName(), Error.CLIENT.getCode(),
				ex.getMessage(), Error.CLIENT.getUserMessage()), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = NoHandlerFoundException.class)
	public ResponseEntity<GlobalErrorDto> handleNoHandlerFoundException(NoHandlerFoundException ex) {
		return new ResponseEntity<GlobalErrorDto>(new GlobalErrorDto(Error.CLIENT.getName(), Error.CLIENT.getCode(),
				ex.getMessage(), Error.CLIENT.getUserMessage()), HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler(value = IllegalArgumentException.class)
	public ResponseEntity<GlobalErrorDto> handleIllegalArgumentException(IllegalArgumentException ex) {
		return new ResponseEntity<GlobalErrorDto>(new GlobalErrorDto(Error.CLIENT.getName(), Error.CLIENT.getCode(),
				ex.getMessage(), Error.CLIENT.getUserMessage()), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<GlobalErrorDto> handleException(Exception ex) {
		logger.error(ex.getMessage());
		return new ResponseEntity<GlobalErrorDto>(new GlobalErrorDto(Error.SERVER.getName(), Error.SERVER.getCode(),
				ex.getMessage(), Error.SERVER.getUserMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
