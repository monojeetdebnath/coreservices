package com.rs.coreservices.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rs.coreservices.dto.response.SubGenreDto;
import com.rs.coreservices.services.SubGenreService;

@RestController
@RequestMapping("/v1/{categoryId}/{genreId}/sub-genres")
public class SubGenreController {

	@Autowired
	private SubGenreService subGenreService;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public List<SubGenreDto> getAllSubGenres(@PathVariable String categoryId, @PathVariable String genreId) {
		return subGenreService.findAll(categoryId, genreId);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public SubGenreDto createSubGenre(@PathVariable String categoryId, @PathVariable String genreId,
			@RequestBody SubGenreDto subGenreDto) {
		return subGenreService.save(categoryId, genreId, subGenreDto);
	}

	@RequestMapping(value = "/{subGenreId}", method = RequestMethod.DELETE)
	public void deleteSubGenre(@PathVariable String categoryId, @PathVariable String genreId,
			@PathVariable String subGenreId) {
		subGenreService.delete(categoryId, genreId, subGenreId);
	}

}