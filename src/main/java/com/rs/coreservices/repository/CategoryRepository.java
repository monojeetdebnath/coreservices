package com.rs.coreservices.repository;

import org.springframework.data.repository.CrudRepository;

import com.rs.coreservices.models.Category;

public interface CategoryRepository extends CrudRepository<Category, String> {

}
