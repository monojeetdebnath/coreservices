package com.rs.coreservices.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.rs.coreservices.models.Genre;

public interface GenreRepository extends CrudRepository<Genre, String> {

	List<Genre> findByCategoryId(String categoryId);

	@Transactional
	void deleteByCategoryIdAndGenreId(String categoryId, String genreId);

}
