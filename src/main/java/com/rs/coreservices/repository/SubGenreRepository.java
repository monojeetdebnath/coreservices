package com.rs.coreservices.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;

import com.rs.coreservices.models.SubGenre;

public interface SubGenreRepository extends CrudRepository<SubGenre, String> {

	List<SubGenre> findByCategoryIdAndGenreId(String categoryId, String genreId);
	
	@Transactional
	void deleteByCategoryIdAndGenreIdAndSubGenreId(String categoryId, String genreId, String subGenreId);

}
