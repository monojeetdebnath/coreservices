package com.rs.coreservices.dto.response;

public class GlobalErrorDto {

	private String errorType;
	private int errorCode;
	private String errorMessage;
	private String userMessage;

	public GlobalErrorDto() {
	}

	public GlobalErrorDto(String errorType, int errorCode, String errorMessage, String userMessage) {
		super();
		this.errorType = errorType;
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.userMessage = userMessage;

	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getUserMessage() {
		return userMessage;
	}

	public void setUserMessage(String userMessage) {
		this.userMessage = userMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}