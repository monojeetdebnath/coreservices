package com.rs.coreservices.dto.response;

public class SubGenreDto {

	private String subGenreId;

	private String subGenreDetails;

	public String getSubGenreId() {
		return subGenreId;
	}

	public void setSubGenreId(String subGenreId) {
		this.subGenreId = subGenreId;
	}

	public String getSubGenreDetails() {
		return subGenreDetails;
	}

	public void setSubGenreDetails(String subGenreDetails) {
		this.subGenreDetails = subGenreDetails;
	}

}
