package com.rs.coreservices.dto.response;

public class GenreDto {

	private String genreId;
	
	private String genreDetails;

	public String getGenreId() {
		return genreId;
	}

	public void setGenreId(String genreId) {
		this.genreId = genreId;
	}

	public String getGenreDetails() {
		return genreDetails;
	}

	public void setGenreDetails(String genreDetails) {
		this.genreDetails = genreDetails;
	}
}
