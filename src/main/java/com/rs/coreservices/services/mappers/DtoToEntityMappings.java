package com.rs.coreservices.services.mappers;

import java.util.List;

import org.mapstruct.Mapper;

import com.rs.coreservices.dto.response.CategoryDto;
import com.rs.coreservices.dto.response.GenreDto;
import com.rs.coreservices.dto.response.SubGenreDto;
import com.rs.coreservices.models.Category;
import com.rs.coreservices.models.Genre;
import com.rs.coreservices.models.SubGenre;

@Mapper(componentModel = "spring")
public interface DtoToEntityMappings {

	Category dtoToCategory(CategoryDto categoryDto);
	
	List<Category> dtoToCategory(List<CategoryDto> categoryDtos);
	
	Genre dtoToGenre(GenreDto genreDto);
	
	List<Genre> dtoToGenre(List<GenreDto> genreDtos);
	
	SubGenre dtoToSubGenre(SubGenreDto subGenreDto);
	
	List<SubGenre> dtoToSubGenre(List<SubGenreDto> subGenreDtos);

}