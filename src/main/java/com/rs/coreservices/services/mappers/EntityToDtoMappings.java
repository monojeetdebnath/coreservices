package com.rs.coreservices.services.mappers;

import java.util.List;

import org.mapstruct.Mapper;

import com.rs.coreservices.dto.response.CategoryDto;
import com.rs.coreservices.dto.response.GenreDto;
import com.rs.coreservices.dto.response.SubGenreDto;
import com.rs.coreservices.models.Category;
import com.rs.coreservices.models.Genre;
import com.rs.coreservices.models.SubGenre;

@Mapper(componentModel = "spring")
public interface EntityToDtoMappings {

	CategoryDto categoryToDto(Category category);
	
	List<CategoryDto> categoryToDto(List<Category> categories);
	
	GenreDto genreToDto(Genre genre);
	
	List<GenreDto> genreToDto(List<Genre> genres);
	
	SubGenreDto subGenreToDto(SubGenre subGenre);
	
	List<SubGenreDto> subGenreToDto(List<SubGenre> subGenres);

}