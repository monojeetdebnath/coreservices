package com.rs.coreservices.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.coreservices.dto.response.CategoryDto;
import com.rs.coreservices.models.Category;
import com.rs.coreservices.repository.CategoryRepository;
import com.rs.coreservices.services.CategoryService;
import com.rs.coreservices.services.mappers.DtoToEntityMappings;
import com.rs.coreservices.services.mappers.EntityToDtoMappings;
import com.rs.coreservices.utils.CoreServiceCollectionUtils;

@Service
public class CategoryServiceImpl implements CategoryService {

	private static final Logger logger = LoggerFactory.getLogger(CategoryServiceImpl.class);

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private DtoToEntityMappings dtoToEntityMappings;

	@Autowired
	private EntityToDtoMappings entityToDtoMappings;

	@Override
	public List<CategoryDto> findAll() {
		return entityToDtoMappings
				.categoryToDto(CoreServiceCollectionUtils.iterableToList(categoryRepository.findAll()));
	}

	@Override
	public CategoryDto findById(String categoryId) {
		return entityToDtoMappings.categoryToDto(categoryRepository.findOne(categoryId));
	}

	@Override
	public CategoryDto save(CategoryDto categoryDto) {
		logger.debug(categoryDto.getCategoryId() + " " + categoryDto.getCategoryDetails());
		Category category = dtoToEntityMappings.dtoToCategory(categoryDto);
		return entityToDtoMappings
				.categoryToDto(categoryRepository.save(category));
	}

	@Override
	public void deleteByCategoryId(String categoryId) {
		categoryRepository.delete(categoryId);
	}

}