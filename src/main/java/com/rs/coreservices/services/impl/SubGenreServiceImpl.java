package com.rs.coreservices.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.coreservices.dto.response.SubGenreDto;
import com.rs.coreservices.models.SubGenre;
import com.rs.coreservices.repository.SubGenreRepository;
import com.rs.coreservices.services.SubGenreService;
import com.rs.coreservices.services.mappers.DtoToEntityMappings;
import com.rs.coreservices.services.mappers.EntityToDtoMappings;

@Service
public class SubGenreServiceImpl implements SubGenreService {

	private static final Logger logger = LoggerFactory.getLogger(SubGenreServiceImpl.class);

	@Autowired
	private SubGenreRepository subGenreRepository;

	@Autowired
	private DtoToEntityMappings dtoToEntityMappings;

	@Autowired
	private EntityToDtoMappings entityToDtoMappings;

	@Override
	public List<SubGenreDto> findAll(String categoryId, String genreId) {
		return entityToDtoMappings.subGenreToDto(subGenreRepository.findByCategoryIdAndGenreId(categoryId, genreId));
	}

	@Override
	public SubGenreDto save(String categoryId, String genreId, SubGenreDto subGenreDto) {
		SubGenre subGenre = dtoToEntityMappings.dtoToSubGenre(subGenreDto);
		subGenre.setCategoryId(categoryId);
		subGenre.setGenreId(genreId);
		return entityToDtoMappings.subGenreToDto(subGenreRepository.save(subGenre));
	}

	@Override
	public void delete(String categoryId, String genreId, String subGenreId) {
		subGenreRepository.deleteByCategoryIdAndGenreIdAndSubGenreId(categoryId, genreId, subGenreId);
	}

}