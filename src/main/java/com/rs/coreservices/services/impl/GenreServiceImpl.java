package com.rs.coreservices.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.coreservices.dto.response.GenreDto;
import com.rs.coreservices.models.Genre;
import com.rs.coreservices.repository.GenreRepository;
import com.rs.coreservices.services.GenreService;
import com.rs.coreservices.services.mappers.DtoToEntityMappings;
import com.rs.coreservices.services.mappers.EntityToDtoMappings;

@Service
public class GenreServiceImpl implements GenreService {

	private static final Logger logger = LoggerFactory.getLogger(GenreServiceImpl.class);

	@Autowired
	private GenreRepository genreRepository;

	@Autowired
	private DtoToEntityMappings dtoToEntityMappings;

	@Autowired
	private EntityToDtoMappings entityToDtoMappings;

	@Override
	public List<GenreDto> findAll(String categoryId) {
		return entityToDtoMappings.genreToDto(genreRepository.findByCategoryId(categoryId));
	}

	@Override
	public GenreDto findById(String genreId) {
		return entityToDtoMappings.genreToDto(genreRepository.findOne(genreId));
	}

	@Override
	public GenreDto save(String categoryId, GenreDto genreDto) {
		Genre genre = dtoToEntityMappings.dtoToGenre(genreDto);
		genre.setCategoryId(categoryId);
		return entityToDtoMappings.genreToDto(genreRepository.save(genre));
	}

	@Override
	public void delete(String categoryId, String genreId) {
		genreRepository.deleteByCategoryIdAndGenreId(categoryId, genreId);
	}

}