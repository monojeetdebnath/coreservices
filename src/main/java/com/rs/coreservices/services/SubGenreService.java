package com.rs.coreservices.services;

import java.util.List;

import com.rs.coreservices.dto.response.SubGenreDto;

public interface SubGenreService {

	List<SubGenreDto> findAll(String categoryId, String genreId);

	SubGenreDto save(String categoryId, String genreId, SubGenreDto subGenreDto);

	void delete(String categoryId, String genreId, String subGenreId);

}