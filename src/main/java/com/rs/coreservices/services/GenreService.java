package com.rs.coreservices.services;

import java.util.List;

import com.rs.coreservices.dto.response.GenreDto;

public interface GenreService {

	List<GenreDto> findAll(String categoryId);

	GenreDto findById(String genreId);

	GenreDto save(String categoryId, GenreDto genreDto);

	void delete(String categoryId, String genreId);

}
