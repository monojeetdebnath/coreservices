package com.rs.coreservices.services;

import java.util.List;

import com.rs.coreservices.dto.response.CategoryDto;

public interface CategoryService {

	List<CategoryDto> findAll();

	CategoryDto findById(String categoryId);

	CategoryDto save(CategoryDto categoryDto);

	void deleteByCategoryId(String categoryId);

}
