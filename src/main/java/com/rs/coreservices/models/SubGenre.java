package com.rs.coreservices.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SUB_GENRE")
public class SubGenre {

	@Id
	@Column(name = "SUB_GENRE_ID")
	private String subGenreId;

	@Column(name = "GENRE_ID")
	private String genreId;

	@Column(name = "CATEGORY_ID")
	private String categoryId;

	@Column(name = "SUB_GENRE_DETAILS")
	private String subGenreDetails;

	public String getSubGenreId() {
		return subGenreId;
	}

	public void setSubGenreId(String subGenreId) {
		this.subGenreId = subGenreId;
	}

	public String getGenreId() {
		return genreId;
	}

	public void setGenreId(String genreId) {
		this.genreId = genreId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getSubGenreDetails() {
		return subGenreDetails;
	}

	public void setSubGenreDetails(String subGenreDetails) {
		this.subGenreDetails = subGenreDetails;
	}

}
