package com.rs.coreservices.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "GENRE")
public class Genre {

	@Id
	@Column(name = "GENRE_ID")
	private String genreId;

	@Column(name = "CATEGORY_ID")
	private String categoryId;

	@Column(name = "GENRE_DETAILS")
	private String genreDetails;

	public String getGenreId() {
		return genreId;
	}

	public void setGenreId(String genreId) {
		this.genreId = genreId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getGenreDetails() {
		return genreDetails;
	}

	public void setGenreDetails(String genreDetails) {
		this.genreDetails = genreDetails;
	}

}
